# VOD Metadata Analytics Service

- [About](#about)
- [Setup](#setup)
- [Usage](#usage)
    - [create-aws-resources](#create-aws-resources)
    - [movie-metadata-uploader](#movie-metadata-uploader)
- [Extra](#extra)
    - [Localhost json2csv testing](#localhost-json2csv-testing)
    - [Lambda function package](#lambda-function-package)
- [TODO](#todo)

## About

* Fetches movies metadata from [OMDB](http://www.omdbapi.com).
* Uploads metadata to an Amazon S3 bucket.
* Convert metadata from JSON to CSV format using a Lambda function.
* Sends a Slack notification if the operation fails.

All components of this project ([create-aws-resources](create-aws-resources), [movie-metadata-uploader](movie-metadata-uploader) and [json2csv.py](lambda-function/json2csv.py)) have been developed using **Python 3.6**.
## Setup

In order to avoid installing dependencies globally in our system, it's highly recommended to run this code in its own Python `virtualenv`.

```
virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt
```

## Usage

#### create-aws-resources

[create-aws-resources](create-aws-resources) uses `boto3` to deploy all the necessary AWS resources.

It performs the following actions:

* Creates S3 bucket where metadata files will be stored.
* Creates IAM role to allow Lambda accessing S3 resources.
* Creates S3 bucket for Lambda function zip package.
* Uploads Lambda package to S3 bucket.
* Creates Lambda function from package.
* Configures S3 Lambda notifications.

This code has been written with reusability in mind. It just deploys a Lambda function that responds to S3 put events so if you want to cover a different use case like another kind of file conversion or image manipulation, it can be done by adjusting the command line arguments accordingly.

```
./create-aws-resources --help
usage: create-aws-resources [-h] --access-key ACCESS_KEY --secret-key
                            SECRET_KEY --region REGION [--s3-bucket S3_BUCKET]
                            [--lambda-s3-role LAMBDA_S3_ROLE]
                            [--lambda-function LAMBDA_FUNCTION]
                            [--lambda-zip-file LAMBDA_ZIP_FILE]
                            [--lambda-runtime LAMBDA_RUNTIME]
                            [--lambda-timeout LAMBDA_TIMEOUT]
                            [--lambda-memory LAMBDA_MEMORY]
                            [--lambda-s3-bucket LAMBDA_S3_BUCKET]
                            [--lambda-s3-key LAMBDA_S3_KEY]
                            [--slack-webhook-url SLACK_WEBHOOK_URL]
                            [--slack-channel SLACK_CHANNEL]

Create a Lambda function that responds to Amazon S3 put events and all
necessary resources in order to do so, including S3 buckets and permissions

optional arguments:
  -h, --help            show this help message and exit
  --access-key ACCESS_KEY
                        AWS access key id (default: None)
  --secret-key SECRET_KEY
                        AWS secret access key (default: None)
  --region REGION       AWS region name (default: None)
  --s3-bucket S3_BUCKET
                        S3 bucket (default: omdb-files)
  --lambda-s3-role LAMBDA_S3_ROLE
                        Lambda S3 execution role (default:
                        lambda-s3-execution-role)
  --lambda-function LAMBDA_FUNCTION
                        Lambda function name (default: json2csv)
  --lambda-zip-file LAMBDA_ZIP_FILE
                        path to Lambda function zip file (default: lambda-
                        function/json2csv.zip)
  --lambda-runtime LAMBDA_RUNTIME
                        Lambda function runtime (default: python3.6)
  --lambda-timeout LAMBDA_TIMEOUT
                        Lambda function timeout (seconds) (default: 15)
  --lambda-memory LAMBDA_MEMORY
                        Lambda function memory size (default: 512)
  --lambda-s3-bucket LAMBDA_S3_BUCKET
                        S3 bucket to store Lambda package (default: json2csv-
                        lambda-package)
  --lambda-s3-key LAMBDA_S3_KEY
                        Lambda package S3 key (default: json2csv.zip)
  --slack-webhook-url SLACK_WEBHOOK_URL
                        Slack webhook URL (default: None)
  --slack-channel SLACK_CHANNEL
                        Slack channel to send notifications (default:
                        #json2csv)
```

The only required arguments are `--access-key` `--secret-key` and `--region`. The output will be like follows:

```
./create-aws-resources --access-key=AWS_ACCESS_KEY_ID --secret-key=AWS_SECRET_ACCESS_KEY --region=AWS_REGION_NAME
* Creating S3 bucket for source files...
(ok) omdb-files created
* Creating Lambda S3 role and permissions...
(ok) lambda-s3-execution-role created
* Creating S3 bucket for lambda package...
(ok) json2csv-lambda-package created
* Uploading Lambda package to S3 bucket...
(ok) json2csv.zip uploaded
* Creating Lambda function from package...
(ok) json2csv created
* Configuring S3 Lambda notifications...
(ok) notifications configured
```
After doing so the AWS infrastructure will be ready to upload some movie metadata and make the file conversion happens.

#### movie-metadata-uploader

[movie-metadata-uploader](movie-metadata-uploader) is a CLI tool which allow us to feed our S3 bucket with some movie metadata in JSON format.

```
./movie-metadata-uploader --help
usage: movie-metadata-uploader [-h] --access-key ACCESS_KEY --secret-key
                               SECRET_KEY --region REGION
                               [--s3-bucket S3_BUCKET]
                               [--n-movies N_MOVIES | --imdb-id IMDB_ID]

Metadata uploader for VOD Analytics Service. Fetches movies metadata from
www.omdbapi.com and uploads them in JSON format to an Amazon S3 bucket.

optional arguments:
  -h, --help            show this help message and exit
  --access-key ACCESS_KEY
                        AWS access key id (default: None)
  --secret-key SECRET_KEY
                        AWS secret access key (default: None)
  --region REGION       AWS region name (default: None)
  --s3-bucket S3_BUCKET
                        S3 bucket to upload OMDb JSON file (default: omdb-
                        files)
  --n-movies N_MOVIES   number of movies to be randomly picked from IMDb Top
                        250 (default: 10)
  --imdb-id IMDB_ID     IMDb movie ID to fetch from OMDb (default: None)
```

Movie metadata is collected based on the **IMDb ID** which is also the unique identifier for movies in [OMDb](http://www.omdbapi.com). So first thing **movie-metadata-uploader** does is to scrape from [IMDb Top 250](http://www.imdb.com/chart/top) looking for those identifiers. Then randomly selects `N` IDs from these 250, fetches them from OMDb and uploads them to our S3 bucket in JSON format. By default `N` is set to 10 but it can be increased to 50 by adjusting `--n-movies=N` argument.

The only required arguments are `--access-key` `--secret-key` and `--region`:

```
./movie-metadata-uploader --access-key=AWS_ACCESS_KEY_ID --secret-key=AWS_SECRET_ACCESS_KEY --region=AWS_REGION_NAME

 --- Metadata uploader for VOD analytics service ---

* Uploading metadata for movie 'Interstellar (2014)'
* Uploading metadata for movie 'The Elephant Man (1980)'
* Uploading metadata for movie 'Vertigo (1958)'
* Uploading metadata for movie 'Witness for the Prosecution (1957)'
* Uploading metadata for movie 'Léon: The Professional (1994)'
* Uploading metadata for movie 'Forrest Gump (1994)'
* Uploading metadata for movie 'Star Wars: Episode V - The Empire Strikes Back (1980)'
* Uploading metadata for movie 'Star Wars: Episode VI - Return of the Jedi (1983)'
* Uploading metadata for movie 'Into the Wild (2007)'
* Uploading metadata for movie 'The Godfather (1972)'

 --- Uploaded metadata for 10 movies in 1.7615458965301514 seconds ---
 ```

**movie-metadata-uploader** also allow us to specify one particular **IMDb ID** by setting the command line argument `--imdb-d=ID`

## Extra

#### Localhost json2csv testing

Folder [json2csv](json2csv-localhost) was used to develop Lambda function locally using some JSON files curled from OMDb.

#### Lambda function package

Lambda function package has been created using [Zappa - Serverless Python](https://github.com/Miserlou/Zappa). This tool creates the lambda function package with all its dependencies when it is executed within the same Python virtualenv that includes such dependencies. One just needs to install it via `pip`.

```
pip install zappa
zappa init
zappa package production -o json2csv.zip
```

Then execute `init` and `package`:

```
zappa init
zappa package production -o json2csv.zip
```

This step **doesn't need to be done**. This section is included only with documentation purpose since the tool used to package the Lambda function is not the recommended in the coding challenge instructions.

##  TODO
* Improve command output by adding loaders for long operations like uploading the Lambda package.
* Add cronjob to reprocess historical data.
* Make **create-aws-resources** idempotent.
* Improve logging.
