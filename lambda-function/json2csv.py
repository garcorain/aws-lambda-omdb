import boto3
import os
import json
from flatten_json import flatten_json
from pandas.io.json import json_normalize
from concurrent.futures import ThreadPoolExecutor
import jsonschema
import requests
import time

MAX_WORKERS = 10
SLACK_WEBHOOK_URL = os.environ["slack_webhook_url"]
SLACK_CHANNEL = os.environ["slack_channel"]

JSON_SCHEMA = json.loads(json.dumps({
    "type": "object",
    "$schema": "http://json-schema.org/draft-03/schema",
    "required": True,
    "properties": {
        "Actors": {
            "type": "string",
            "required": True
            },
        "Awards": {
            "type": "string",
            "required": True
        },
        "BoxOffice": {
            "type": "string",
            "required": True
        },
        "Country": {
            "type": "string",
            "required": True
            },
        "DVD": {
            "type": "string",
            "required": True
        },
        "Director": {
            "type": "string",
            "required": True
        },
        "Genre": {
            "type": "string",
            "required": True
        },
        "Language": {
            "type": "string",
            "required": True
        },
        "Metascore": {
            "type": "string",
            "required": True
        },
        "Plot": {
            "type": "string",
            "required": True
        },
        "Poster": {
            "type": "string",
            "required": True
        },
        "Production": {
            "type": "string",
            "required": True
        },
        "Rated": {
            "type": "string",
            "required": True
        },
        "Ratings": {
            "type": "array",
            "required": True,
            "items": {
                "type": "object",
                "required": True,
                "properties": {
                    "Source": {
                        "type": "string",
                        "required": True
                    },
                    "Value": {
                        "type": "string",
                        "required": True
                    }
                }
            }
        },
        "Released": {
            "type": "string",
            "required": True
        },
        "Response": {
            "type": "string",
            "required": True
        },
        "Runtime": {
            "type": "string",
            "required": True
        },
        "Title": {
            "type": "string",
            "required": True
        },
        "Type": {
            "type": "string",
            "required": True
        },
        "Website": {
            "type": "string",
            "required": True
        },
        "Writer": {
            "type": "string",
            "required": True
        },
        "Year": {
            "type": "string",
            "required": True
        },
        "imdbID": {
            "type": "string",
            "required": True
        },
        "imdbRating": {
            "type": "string",
            "required": True
        },
        "imdbVotes": {
            "type": "string",
            "required": True
        }
    }
}))

s3 = boto3.client("s3")


def slack_notification(message):
    payload = {
        "attachments": [
            {
                "color": "#e50000",
                "author_name": "json2csv Lambda function",
                "title": "Error processing S3 file",
                "text": message,
                "ts": time.time()
            }
        ],
        "channel": SLACK_CHANNEL,
        "icon_emoji": ":file_folder:"
    }

    r = requests.post(
        SLACK_WEBHOOK_URL,
        data=json.dumps(payload),
        headers={'Content-Type': 'application/json'}
    )

    if r.status_code != 200:
        raise ValueError(
            "slack_notification error: {} - {}".format(r.status_code, r.text)
        )

    return


def json2csv(bucket, key, csv_key, tmp_download, tmp_upload):
    s3.download_file(bucket, key, tmp_download)

    with open(tmp_download, "r") as fin, open(tmp_upload, "w") as fout:

        f = fin.name.split("/")[2]

        try:
            json_data = json.load(fin)
        except json.JSONDecodeError as e:
            msg = "{} doesn't seem to be a JSON file\nJSONDecodeError: {}".format(f, e)
            print(msg)
            if SLACK_WEBHOOK_URL is not "None":
                slack_notification(msg)
            return

        try:
            jsonschema.Draft3Validator(JSON_SCHEMA).validate(json_data)
        except jsonschema.ValidationError as e:
            msg = "{} doesn't match OMDb JSON schema\nValidationError: {}".format(f, e)
            print(msg)
            if SLACK_WEBHOOK_URL is not "None":
                slack_notification(msg)
            return

        data = flatten_json(json_data)
        csvdata = json_normalize(data).to_csv(index=False)
        fout.write(csvdata)

    s3.upload_file(tmp_upload, bucket, csv_key)


def handler(event, context):
    for record in event["Records"]:
        bucket = record["s3"]["bucket"]["name"]
        key = record["s3"]["object"]["key"]
        csv_key = "csv/{}".format(key.replace(".json", ".csv"))
        tmp_download = "/tmp/{}".format(key)
        tmp_upload = "/tmp/{}".format(key.replace(".json", ".csv"))
        with ThreadPoolExecutor(max_workers=MAX_WORKERS) as x:
            x.submit(json2csv, bucket, key, csv_key, tmp_download, tmp_upload)
