#!/usr/bin/env python3

from os import listdir
import json
from flatten_json import flatten_json
from pandas.io.json import json_normalize
from threading import Thread
import time

start_time = time.time()

JSON_PATH = "json"
CSV_PATH = "csv"


def json2csv(f):
    jsonfile = "{}/{}".format(JSON_PATH, f)
    csvfile = "{}/{}".format(CSV_PATH, f.replace(".json", ".csv"))
    with open(jsonfile, "r") as fin, open(csvfile, "w") as fout:
        data = flatten_json(json.load(fin))
        csvdata = json_normalize(data).to_csv(index=False)
        fout.write(csvdata)


filenames = listdir(JSON_PATH)

for f in filenames:
    t = Thread(target=json2csv, args=(f,)).start()

duration = time.time() - start_time

print("{} json files processed in {} seconds".format(len(filenames), duration))
